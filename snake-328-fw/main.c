#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include "font.h"

/**
    PB2 - CHIP ENABLE
    PD6 - RESET
    PD7 - D/!C
    PD6 - backlight
*/

#define ENABLE_DISP  PORTB&=~(1<<PB2);
#define DISABLE_DISP PORTB|=(1<<PB2);

#define DISP_DATA PORTD|=(1<<PD7);
#define DISP_COMMAND PORTD&=~(1<<PD7);

//KEYS
#define KEY_LEFT    (1 << 0)
#define KEY_RIGHT   (1 << 3)
#define KEY_UP      (1 << 1)
#define KEY_DOWN    (1 << 2)
#define KEY_R_FUNC  (1 << 5)
#define KEY_L_FUNC  (1 << 4)

volatile uint8_t video_RAM[6][84],
                sending_ram = 0,
                ram_x = 0,
                ram_y = 0,
                button = 0,
                down;

volatile uint16_t r_x, r_y;

int8_t car_x = 0,
        car_y = 0;

uint16_t i = 0;
uint8_t j = 0;

uint8_t high_score_addresses[2][3] = {
        {10, 16, 22},
        {28, 34, 40}
    };

void clear_video_ram() {
    uint8_t j, i;
    for(i = 0; i < 6; i++) {
        for(j = 0; j < 84; j++) {
            video_RAM[i][j] = 0b00000000;
        }
    }
}

//low-level graphics functions
void pixel_on(int8_t x, int8_t y) {
    video_RAM[y/8][x] |= (1 << (y%8));
}

void pixel_off(int8_t x, int8_t y) {
    video_RAM[y/8][x] &= ~(1 << (y%8));
}

void pixel_toggle(int8_t x, int8_t y) {
    video_RAM[y/8][x] ^= (1 << (y%8));
}

uint8_t pixel_is(int8_t x, int8_t y) {
    return (video_RAM[y/8][x] & (1 << (y%8)));
}

void invert_pixels(int8_t s_x, int8_t s_y, int8_t e_x, int8_t e_y) {
    uint8_t i, j;

    for(i = 0; i < (e_y - s_y); i++) {
        for(j = 0; j < (e_x - s_x); j++) {
            pixel_toggle(s_x + j, s_y + i);
        }
    }
}

//text functions
/**Print text to video_ram*/
void print(char text[]) {
    uint8_t i = 0, j, tmp_char;

    while((tmp_char = (uint8_t)text[i]) > 0) {
        i++;
        if(tmp_char == 10) {
            car_x = 0;
            car_y++;
            if(car_y > 5) {
                car_y = 0;
            }
        } else {
            for(j = 0; j < 5; j++) {
                video_RAM[car_y][(car_x * 6) + j] = pgm_read_byte(&font[tmp_char - 32][j]);
            }
            car_x++;
            if(car_x > 13) {
                car_x = 0;
                car_y++;
                if(car_y > 5) {
                    car_y = 0;
                }
            }
        }
    }
}

/**Start printing text from 0,0*/
void print_d(char text[]) {
    //reset carriage position
    car_x = 0;
    car_y = 0;
    print(text);
}

/**Append text from last position*/
void print_a(char text[]) {
    print(text);
}

/**Start printing text from x,y, if out of range, it append*/
void print_s(char text[], int8_t x, int8_t y) {
    if((x >= 0) && (x <= 13)) {
        car_x = x;
    }
    if((y >= 0) && (x <= 5)) {
        car_y = y;
    }
    print(text);
}


//Keyboard debounce timer - timer 0
ISR(TIMER0_OVF_vect) {
    TCCR0B &= ~((1 << CS02) | (1 << CS00)); //Stop timer
    TCNT0 = 0; //Reset timer

    if((~PINC & 0b00111111) == button) {
        GPIOR0 = button;
    }
    button = 0;
}

//Keyboard interrupt
ISR(PCINT1_vect) {
    if(button == 0) {
        button = (~PINC & 0b00111111);
        if(button) {
            TCCR0B |= (1 << CS02) | (1 << CS00); //start debounce timer - 32 ms
        }
    }
}

//SPI - video RAM to display sending routine
ISR(SPI_STC_vect) {
    if(sending_ram == 1) {
        SPDR = video_RAM[ram_y][ram_x++];
        if(ram_x == 84) {
            ram_x = 0;
            ram_y++;
            if(ram_y == 6) {
                ram_y = 0;
                ram_x = 0;
                sending_ram = 0;
            }
        }
    }
}

//sending video to display start timer
ISR(TIMER1_CAPT_vect) {
    SPDR = video_RAM[ram_y][ram_x++];
    sending_ram = 1;
}

//Snake
uint8_t snake_menu(void) {
    static int8_t start = 1, items = 3, left_arrow = 2, positions[3] = {6,12,7};
    int8_t selected = 0;

    clear_video_ram();

    print_s("Hrat", left_arrow + 1, start);
    print_s("Nej skore", left_arrow + 1, start + 1);
    print_s("Zpet", left_arrow + 1, start + 2);
    //print_s("Zpet", left_arrow + 1, start + 3);
    //print_s("O me", left_arrow + 1, start + 4);
    //print_s("O me", left_arrow + 1, start + 5);

    print_s(">", left_arrow, start);
    print_s("<", positions[selected], start);

    while(1) {
        //READING KEYBOARD
        if(GPIOR0 > 0) {
            switch(GPIOR0) {
                case KEY_LEFT : {

                    break;
                }

                case KEY_RIGHT : {

                    break;
                }

                case KEY_UP : {
                    selected--;
                    if(selected < 0) {
                        selected = items - 1;
                        print_s(" ", left_arrow, start);
                        print_s(" ", positions[0], start);
                    } else {
                        print_s(" ", left_arrow, selected + 1 + start);
                        print_s(" ", positions[selected + 1], selected + 1 + start);
                    }
                    print_s(">", left_arrow, selected + start);
                    print_s("<", positions[selected], selected + start);
                    break;
                }

                case KEY_DOWN : {
                    selected++;
                    if(selected > items - 1) {
                        selected = 0;
                        print_s(" ", left_arrow, items - 1 + start);
                        print_s(" ", positions[items - 1], items - 1 + start);
                    } else {
                        print_s(" ", left_arrow, selected - 1 + start);
                        print_s(" ", positions[selected - 1], selected - 1 + start);
                    }
                    print_s(">", left_arrow, selected + start);
                    print_s("<", positions[selected], selected + start);
                    break;
                }

                case KEY_R_FUNC : {
                    break;
                }

                case KEY_L_FUNC : {
                    GPIOR0 = 0; //Clear after read!
                    return selected;
                }
            }
            GPIOR0 = 0; //Clear after read!
        }
    }
}

/** Draw 3x3 pixel block in standard coord. system*/
void draw_block_pattern(uint8_t x_bl, uint8_t y_bl, uint16_t pattern) {
    uint8_t offset_x = (x_bl * 4) + 2;
    uint8_t offset_y = (y_bl * 4) + 4;

    uint8_t i = 0, r = 0;
    for(; i < 9; i++) {
        r = i/3;
        if(pattern & (1 << i)) {
                //   x y
            pixel_on(offset_x + i - r*3, offset_y - r);
        } else {
            pixel_off(offset_x + i - r*3, offset_y - r);
        }
    }
}

uint16_t RNG1() {
    r_x = (uint16_t)(((uint32_t)r_x * (uint32_t)931 + (uint32_t)12641) % (uint32_t)44640);
    return r_x;
}

uint16_t RNG2() {
    r_y = (uint16_t)(((uint32_t)r_y * (uint32_t)40 + (uint32_t)9029) % (uint32_t)41067);
    return r_y;
}

void draw_con(uint8_t x_bl, uint8_t y_bl, uint8_t move_dir) {
    uint8_t offset_x = (x_bl * 4) + 2;
    uint8_t offset_y = (y_bl * 4) + 4;

    /**
        STOP = 0
        RIGHT = 1
        DOWN = 2
        LEFT = 3
        UP = 4
    */
    uint8_t i = 0;

    switch(move_dir) {
        case 3 : { //
            offset_x += 3;

            for(; i < 3; i++) {
                pixel_on(offset_x, offset_y - i);
            }
            break;
        }

        case 1 : {
            offset_x -= 1;

            for(; i < 3; i++) {
                pixel_on(offset_x, offset_y - i);
            }
            break;
        }

        case 4 : {
            offset_y += 1;

            for(; i < 3; i++) {
                pixel_on(offset_x + i, offset_y);
            }
            break;
        }

        case 2 : {
            offset_y -= 3;

            for(; i < 3; i++) {
                pixel_on(offset_x + i, offset_y);
            }
            break;
        }
    }
}

void del_con(uint8_t x_bl, uint8_t y_bl, uint8_t move_dir) {
    uint8_t offset_x = (x_bl * 4) + 2;
    uint8_t offset_y = (y_bl * 4) + 4;

    /**
        STOP = 0
        RIGHT = 1
        DOWN = 2
        LEFT = 3
        UP = 4
    */
    uint8_t i = 0;
    switch(move_dir) {
        case 3 : { //
            offset_x -= 1;

            for(; i < 3; i++) {
                pixel_off(offset_x, offset_y - i);
            }
            break;
        }

        case 1 : {
            offset_x += 3;

            for(; i < 3; i++) {
                pixel_off(offset_x, offset_y - i);
            }
            break;
        }

        case 4 : {
            offset_y -= 3;

            for(; i < 3; i++) {
                pixel_off(offset_x + i, offset_y);
            }
            break;
        }

        case 2 : {
            offset_y += 1;

            for(; i < 3; i++) {
                pixel_off(offset_x + i, offset_y);
            }
            break;
        }
    }
}

uint8_t test_con(uint8_t x_bl, uint8_t y_bl) {
    uint8_t offset_x = (x_bl * 4) + 2;
    uint8_t offset_y = (y_bl * 4) + 4;

    /**
        STOP = 0
        RIGHT = 1
        DOWN = 2
        LEFT = 3
        UP = 4
    */
    if(pixel_is(offset_x - 1, offset_y)) {
        return 3;
    } else if(pixel_is(offset_x + 3, offset_y)) {
        return 1;
    } else if(pixel_is(offset_x, offset_y - 3)) {
        return 4;
    } else if(pixel_is(offset_x, offset_y + 1)) {
        return 2;
    }
    return 0;
}

uint8_t is_snake(uint8_t x_bl, uint8_t y_bl) {
    if(x_bl >= 0 && x_bl < 20) {
        if(y_bl >= 0 && y_bl < 11) {
            return pixel_is((x_bl * 4) + 2, (y_bl * 4) + 4);
        }
    }
    return 0;
}

uint8_t is_block(uint8_t x_bl, uint8_t y_bl) {
    if(x_bl >= 0 && x_bl < 20) {
        if(y_bl >= 0 && y_bl < 10) {
            return pixel_is((x_bl * 4) + 2, (y_bl * 4) + 4);
        }
    }
    return 0;
}

uint8_t pause(uint16_t score) {
    uint8_t return_code = 2;

    // Stop video refresh timer
    TCCR1B &= ~((1 << CS12) | (1 << CS10));
    TCNT1 = 434;

    //wait to end of sending data to display
    while(sending_ram == 1);

    //disable SPI interrupt
    cli();
    SPCR &= ~(1 << SPIE);
    sei();


    //construct pause screen
    uint8_t disp[60] =
            {
                0x7F, 0x09, 0x09, 0x09, 0x06, 0,
                0x7E, 0x11, 0x11, 0x11, 0x7E, 0,
                0x3F, 0x40, 0x40, 0x40, 0x3F, 0,
                0x46, 0x49, 0x49, 0x49, 0x31, 0,
                0x7F, 0x49, 0x49, 0x49, 0x41, 0, //End of first line
                0,0,0,0,0,0,
                0,0,0,0,0,0,
                0,0,0,0,0,0,
                0,0,0,0,0,0,
                0,0,0,0,0,0
            };

    uint16_t i = 0;
    uint8_t k = 0;

    char cislo[4];
    cislo[0] = (score / 1000) + 0x30;
    score -= ((score / 1000) * 1000);

    cislo[1] = (score / 100) + 0x30;
    score -= ((score / 100) * 100);

    cislo[2] = (score / 10) + 0x30;
    score -= ((score / 10) * 10);

    cislo[3] = score + 0x30;


    for(i = 0; i < 4; i++) {
        for(k = 0; k < 5; k++) {
            disp[35 + (i * 6) + k] = pgm_read_byte(&font[cislo[i] - 32][k]);
        }
    }

    k = 0;
    for(i = 0; i < 504; i++) {
        if(i > 192 && i < 222) { //write first line
            SPDR = disp[k++];
        } else if(i > 276 && i < 306) { //write second line
            SPDR = disp[k++];
        } else {
            SPDR = 0b00000000;
        }

        while(!(SPSR & (1<<SPIF)));
    }


    while(return_code == 2) {
        switch(GPIOR0) {
            case KEY_L_FUNC : {
                return_code = 1;
                break;
            }

            case KEY_R_FUNC : {
                return_code = 0;
                break;
            }
        }
    }
    GPIOR0 = 0;

    // Start again video refresh timer
    TCCR1B |= (1 << CS12) | (1 << CS10);

    //re-enable SPI interrupt
    cli();
    SPCR |= (1 << SPIE);
    sei();

    return return_code;
}

void new_high_score(uint16_t score, uint8_t game){
    uint16_t scores[3];
    char names[3][4];

    int8_t i, l;
    uint8_t better = 3, inter = 1;

    for(i = 0; i < 3; i++) {
        scores[i] = eeprom_read_word((uint16_t *) (uint16_t)high_score_addresses[game][i]);

        if(score > scores[i] && i < better) {
            better = i;
        }

        for(l = 0; l < 4; l++) {
            names[i][l] = eeprom_read_byte((uint8_t *) (uint8_t)high_score_addresses[game][i] + 2 + l);
        }
    }

    if(better < 3) {
        int8_t pos = 0;
        uint8_t blink_counter = 0;

        char new_name[5] = "AAAA";

        clear_video_ram();
        print_d("New high score!\nYour name:\n");

        print_s(new_name, 0, 3);

        while(inter) {
            switch(GPIOR0) {
                case KEY_R_FUNC : {
                    inter = 0;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_LEFT : {
                    pos--;
                    if(pos < 0) {
                        pos = 3;
                    }

                    print_s(new_name, 0, 3);
                    print_s("_", pos, 3);
                    blink_counter = 0;

                    GPIOR0 = 0;
                    break;
                }

                case KEY_RIGHT : {
                    pos++;
                    if(pos > 3) {
                        pos = 0;
                    }

                    print_s(new_name, 0, 3);
                    print_s("_", pos, 3);
                    blink_counter = 0;

                    GPIOR0 = 0;
                    break;
                }

                case KEY_UP : {
                    new_name[pos]++;

                    if(new_name[pos] > 'Z') {
                        new_name[pos] = 'A';
                    }

                    print_s(new_name, 0, 3);
                    blink_counter = 0;

                    GPIOR0 = 0;
                    break;
                }

                case KEY_DOWN : {
                    new_name[pos]--;

                    if(new_name[pos] < 'A') {
                        new_name[pos] = 'Z';
                    }

                    print_s(new_name, 0, 3);
                    blink_counter = 0;

                    GPIOR0 = 0;
                    break;
                }

            }


            blink_counter++;
            if(blink_counter == 25) {
                print_s("_", pos, 3);
            }

            if(blink_counter == 50) {
                print_s(new_name, 0, 3);
                blink_counter = 0;
            }

            _delay_ms(10);

        }

        for(i = better; i < 2; i++) {
            eeprom_write_word((uint16_t *) (uint16_t)high_score_addresses[game][i + 1], scores[i]);

            for(l = 0; l < 4; l++) {
                eeprom_write_byte((uint8_t *) (uint8_t)high_score_addresses[game][i + 1] + 2 + l, names[i][l]);
            }
        }

        eeprom_write_word((uint16_t *) (uint16_t)high_score_addresses[game][better], score);

        for(l = 0; l < 4; l++) {
            eeprom_write_byte((uint8_t *) (uint8_t)high_score_addresses[game][better] + 2 + l, new_name[l]);
        }

    }


}

void game_over(uint16_t score, uint8_t game) {
    clear_video_ram();

    print_s("GAME OVER", 2, 1);
    print_s("SCORE:", 4, 2);

    char cislo[5];
    uint16_t tmp_c = score;

    cislo[0] = (tmp_c / 1000) + 0x30;
    tmp_c -= ((tmp_c / 1000) * 1000);

    cislo[1] = (tmp_c / 100) + 0x30;
    tmp_c -= ((tmp_c / 100) * 100);

    cislo[2] = (tmp_c / 10) + 0x30;
    tmp_c -= ((tmp_c / 10) * 10);

    cislo[3] = tmp_c + 0x30;

    cislo[4] = 0;

    print_s(cislo, 5, 3);

    while(GPIOR0 != KEY_R_FUNC);
    GPIOR0 = 0;

    new_high_score(score, game);
}

void snake_main() {
    clear_video_ram();

    uint8_t speed[] = {66, 33, 25, 20, 16, 14};
    uint8_t lvl_tresh[] = {7, 30, 50, 75, 150, 255};
    uint8_t lvl = 0, dead = 0;

    //uint8_t speed_t = speed[lvl];

    uint8_t dir = 0, tail_dir = 0;
    uint8_t speed_counter = 0;

    uint8_t h_x = 2, h_y = 0; //head
    uint8_t t_x = 0, t_y = 0; //tail - last segment

    uint8_t f_x, f_y;

    uint8_t score = 0;

    // Draw game place lines
    uint8_t i;
    for(i = 0; i < 82; i++) { pixel_on(i, 0); }
    for(i = 0; i < 83; i++) { pixel_on(i, 46); }

    for(i = 0; i < 46; i++) { pixel_on(0, i); }
    for(i = 0; i < 46; i++) { pixel_on(82, i); }

    /**
        STOP = 0
        RIGHT = 1
        DOWN = 2
        LEFT = 3
        UP = 4
    */

    //Init snake body
    draw_block_pattern(h_x, h_y, 0x01FF);
    draw_con(h_x, h_y, 1);

    draw_block_pattern(h_x - 1, h_y, 0x01FF);
    draw_con(h_x - 1, h_y, 1);

    draw_block_pattern(t_x, t_y, 0x01FF);

    //Init food
    do{
        do {
            f_x = (uint16_t)RNG1()/(uint16_t)2232;
        } while(f_y > 19);
        do {
            f_y = (uint16_t)RNG2()/(uint16_t)3733;
        } while(f_y > 10);
    } while(is_snake(f_x, f_y));

    draw_block_pattern(f_x, f_y, 0x00AA);

    while(1) {

        //delay for counting speed
        _delay_ms(10);
        speed_counter++;

        if(speed_counter == speed[lvl]) { //speed defined by level
        //if(speed_counter == speed_t) { //speed defined by level
            speed_counter = 0;

            //change head position
            /**
                STOP = 0
                RIGHT = 1
                DOWN = 2
                LEFT = 3
                UP = 4
            */
            switch(dir) {
                case 3 : {
                    if(h_x == 0) {
                        dead = 1;
                        break;
                    }
                    h_x -= 1;
                    break;
                }

                case 1 : {
                    if(h_x == 19) {
                        dead = 1;
                        break;
                    }
                    h_x += 1;
                    break;
                }

                case 4 : {
                    if(h_y == 0) {
                        dead = 1;
                        break;
                    }
                    h_y -= 1;
                    break;
                }

                case 2 : {
                    if(h_y == 10) {
                        dead = 1;
                        break;
                    }
                    h_y += 1;
                    break;
                }
            }


            if(dir != 0) {
                dead = is_snake(h_x, h_y);

                //END GAME if snake is dead
                if(dead) {
                    game_over(score * 10, 0);
                    return;
                }


                //redraw routine - add head
                draw_block_pattern(h_x, h_y, 0x01FF);
                draw_con(h_x, h_y, dir);

                //CHECK food
                if(h_x == f_x && h_y == f_y) {
                    //Eaten
                    do{
                        do {
                            f_x = (uint16_t)RNG1()/(uint16_t)2232;
                        } while(f_y > 19);
                        do {
                            f_y = (uint16_t)RNG2()/(uint16_t)3733;
                        } while(f_y > 10);
                    } while(is_snake(f_x, f_y));

                    draw_block_pattern(f_x, f_y, 0x00AA);

                    score += 1;

                    if(score == lvl_tresh[lvl]) {
                        lvl++;
                    }
                    /*if((score % 3) == 0 && (speed_t > 140)) {
                        speed_t -= 1;
                    }*/
                } else {
                    //redraw routine - delete tail
                    draw_block_pattern(t_x, t_y, 0x0000);

                    tail_dir = test_con(t_x, t_y);
                    del_con(t_x, t_y, tail_dir);

                    //change tail position
                    /**
                        STOP = 0
                        RIGHT = 1
                        DOWN = 2
                        LEFT = 3
                        UP = 4
                    */
                    switch(tail_dir) {
                        case 3 : {
                            t_x -= 1;
                            break;
                        }

                        case 1 : {
                            t_x += 1;
                            break;
                        }

                        case 4 : {
                            t_y -= 1;
                            break;
                        }

                        case 2 : {
                            t_y += 1;
                            break;
                        }
                    }

                    /*
                    char cislo[6];

                    cislo[0] = (t_x / 10) + 0x30;
                    cislo[1] = (t_x - ((t_x / 10) * 10)) + 0x30;

                    cislo[2] = ':';

                    cislo[3] = (t_y / 10) + 0x30;
                    cislo[4] = (t_y - ((t_y / 10) * 10)) + 0x30;

                    cislo[5] = 0;

                    print_s(cislo, 0, 5);
                    */
                }
            }

        }

        // Keyboard handling
        if(GPIOR0 > 0) {
            switch(GPIOR0) {
                case KEY_LEFT : {
                    dir = 3;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_RIGHT : {
                    dir = 1;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_UP : {
                    dir = 4;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_DOWN : {
                    dir = 2;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_L_FUNC : {
                    GPIOR0 = 0; //Clear after read!

                    if(!pause((uint16_t)score * 10)) {
                        return;
                    }

                    break;
                }

                case KEY_R_FUNC : {
                    GPIOR0 = 0; //Clear after read!

                    /*speed_counter = 0;
                    lvl++;
                    if(lvl == 5) {
                        lvl = 0;
                    }
                    */

                    /*
                    GPIOR0 = 0;
                    uint8_t x = 0, y = 0;
                    while(1) {
                        if(GPIOR0 > 0) {
                            pixel_off(x,y);
                            switch(GPIOR0) {
                                case KEY_LEFT : {
                                    x -= 1;
                                    GPIOR0 = 0;
                                    break;
                                }

                                case KEY_RIGHT : {
                                    x += 1;
                                    GPIOR0 = 0;
                                    break;
                                }

                                case KEY_UP : {
                                    y -= 1;
                                    GPIOR0 = 0;
                                    break;
                                }

                                case KEY_DOWN : {
                                    y += 1;
                                    GPIOR0 = 0;
                                    break;
                                }

                                default : {
                                    GPIOR0 = 0;
                                    return;
                                }
                            }
                            pixel_on(x,y);
                        }
                    }

                    break;
                    */
                }
            }
        }
    }

/*
    draw_block_pattern(0, 0, 0x0054);
    draw_block_pattern(3, 1, 0x01AB);
    draw_block_pattern(5, 2, 0x00AA);
    draw_block_pattern(7, 3, 0x01FF);

    uint8_t x = 0, y = 0;
    while(1) {
        if(GPIOR0 > 0) {
            pixel_off(x,y);
            switch(GPIOR0) {
                case KEY_LEFT : {
                    x -= 1;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_RIGHT : {
                    x += 1;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_UP : {
                    y -= 1;
                    GPIOR0 = 0;
                    break;
                }

                case KEY_DOWN : {
                    y += 1;
                    GPIOR0 = 0;
                    break;
                }

                default : {
                    GPIOR0 = 0;
                    return;
                }
            }
            pixel_on(x,y);
        }
    }
    */
}

void tetris_main() {
    //x of tetris segment is y of snake segment
    uint16_t score = 0;

    uint8_t i, k, run, speed_counter = 0, lvl = 1, collision, lvl_counter = 0;

    uint8_t speed[] = {75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10}; //14

    //block choosing
    int8_t active_block[4][2];
    uint8_t r1, r2;
    uint8_t choose_block[49] = {6,4,1,1,1,1,6,5,4,0,4,0,3,0,2,4,0,2,5,3,4,5,0,5,6,4,0,1,2,6,3,3,1,4,6,2,3,3,1,3,6,6,5,5,2,2,5,2,0};

    //rotation
    int8_t x_tmp, y_tmp, x_tmp1, y_tmp1, x_dif, y_dif;
    int8_t tmp_block[4][2];

    //full row finding
    uint8_t full, l;

    uint8_t init_position[7][4][2] = {
        { //I
            {4, 19},
            {3, 19},
            {5, 19},
            {6, 19}
        },
        { //O
            {4, 19},
            {4, 18},
            {5, 19},
            {5, 18}
        },
        { //J
            {5, 18},
            {4, 19},
            {4, 18},
            {6, 18}
        },
        { //L
            {5, 18},
            {4, 18},
            {6, 18},
            {6, 19}
        },
        { //T
            {5, 18},
            {5, 19},
            {4, 18},
            {6, 18}
        },
        { //S
            {5, 19},
            {6, 19},
            {4, 18},
            {5, 18}
        },
        { //Z
            {5, 19},
            {4, 19},
            {5, 18},
            {6, 18}
        }
    };

    clear_video_ram();

    // Draw game place lines
    for(i = 0; i < 82; i++) { pixel_on(i, 0); }
    for(i = 0; i < 83; i++) { pixel_on(i, 42); }

    for(i = 0; i < 42; i++) { pixel_on(0, i); }
    for(i = 0; i < 42; i++) { pixel_on(82, i); }


    //set active block
    uint8_t A = 0;
    while(1) {

        //random block choose
        do{
            r1 = 10 + RNG1()/(uint16_t)731;
        } while(r1 > 70);

        do{
            r2 = 10 + RNG2()/(uint16_t)673;
        } while(r2 > 70);

        A = choose_block[(r1 * r2) / 100];

        for(i = 0; i < 4; i++) {
            if(!is_block(init_position[A][i][1], init_position[A][i][0])) { //if is place clear, put tehre new piece
                for(k = 0; k < 2; k++) {
                    active_block[i][k] = init_position[A][i][k];
                }

                draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
            } else {
                //END OF GAME - new block collided with already placed
                game_over(score, 1);
                return;
            }
        }

        run = 1;
        while(run) {
            _delay_ms(10);
            speed_counter++;

            //movement of block
            if(speed_counter >= speed[lvl - 1]) {
                speed_counter = 0;

                for(i = 0; i < 4; i++) { //delete block
                    if(active_block[i][1] < 20) {
                        draw_block_pattern(active_block[i][1], active_block[i][0], 0x0000);
                    }
                }

                for(i = 0; i < 4; i++) { //test for collision with other block
                    if(is_block(active_block[i][1] - 1, active_block[i][0])) {
                        run = 0;
                    }
                }

                if(run == 1) { //draw block
                    for(i = 0; i < 4; i++) { //at new position if available
                        active_block[i][1] -= 1;
                        if(active_block[i][1] < 20) {
                            draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
                        }
                    }
                } else {
                    for(i = 0; i < 4; i++) { //at the same position
                        if(active_block[i][1] < 20) {
                            draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
                        }
                    }
                }

                if(run == 0) { //if block cant move anymore
                    break;
                }

                for(i = 0; i < 4; i++) { //if block is at bottom of screen
                    if(active_block[i][1] == 0) {
                        run = 0;
                        break;
                    }
                }
            }

            // Keyboard handling
            if(GPIOR0 > 0) {
                switch(GPIOR0) {
                    case KEY_LEFT : { //DOWN
                        speed_counter = speed[lvl];

                        GPIOR0 = 0;
                        break;
                    }

                    case KEY_RIGHT : { //UP
                        collision = 0;

                        //only rotatable pieces
                        if(A != 1) {
                            for(i = 0; i < 4; i++) { //delete block
                                if(active_block[i][1] < 20) {
                                    draw_block_pattern(active_block[i][1], active_block[i][0], 0x0000);
                                }
                            }

                            //translation vector
                            x_dif = active_block[0][0];
                            y_dif = active_block[0][1];

                            for(i = 0; i < 4; i++) {//rotation
                                //center piece
                                x_tmp = (int8_t)active_block[i][0] - x_dif;
                                y_tmp = (int8_t)active_block[i][1] - y_dif;

                                //rotation
                                x_tmp1 = y_tmp + x_dif;
                                y_tmp1 = (-1 * x_tmp) + y_dif;

                                //move piece back
                                tmp_block[i][0] = x_tmp1;
                                tmp_block[i][1] = y_tmp1;
                            }

                            do { //if block try to rotate near border, it needs to be moved to free space
                                switch(collision) { //move block, depend on with which border it colides
                                    case 2 : { //left border
                                        for(i = 0; i < 4; i++) {
                                            tmp_block[i][0] += 1;
                                        }
                                        break;
                                    }

                                    case 3 : { //right border
                                        for(i = 0; i < 4; i++) {
                                            tmp_block[i][0] -= 1;
                                        }
                                        break;
                                    }
                                }

                                collision = 0;

                                for(i = 0; i < 4; i++) { //test for collision with other block or border
                                    if((tmp_block[i][0]) < 0) { //left
                                        collision = 2;
                                    }
                                    if((tmp_block[i][0]) > 9) { //right
                                        collision = 3;
                                    }
                                    if(is_block(tmp_block[i][1], tmp_block[i][0])) { //other block
                                        collision = 1;
                                    }
                                }
                            } while(collision > 1); //before collision resolve

                            if(collision == 0) {//set piece after rotation as active
                                for(i = 0; i < 4; i++) {
                                    active_block[i][0] = tmp_block[i][0];
                                    active_block[i][1] = tmp_block[i][1];
                                }
                            }

                            for(i = 0; i < 4; i++) { //draw active piece
                                if(active_block[i][1] < 20) {
                                    draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
                                }
                            }
                        }
                        GPIOR0 = 0;
                        break;
                    }

                    case KEY_UP : { //LEFT
                        collision = 0;

                        for(i = 0; i < 4; i++) { //delete block
                            if(active_block[i][1] < 20) {
                                draw_block_pattern(active_block[i][1], active_block[i][0], 0x0000);
                            }
                        }

                        for(i = 0; i < 4; i++) { //test for collision with other block or border
                            if(is_block(active_block[i][1], active_block[i][0] - 1)) {
                                collision = 1;
                            }
                            if((active_block[i][0] - 1) < 0) {
                                collision = 1;
                            }
                        }

                        if(collision == 0) { //draw block
                            for(i = 0; i < 4; i++) { //at new position if available
                                active_block[i][0] -= 1;
                                if(active_block[i][1] < 20) {
                                    draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
                                }
                            }
                        } else {
                            for(i = 0; i < 4; i++) { //at the same position
                                if(active_block[i][1] < 20) {
                                    draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
                                }
                            }
                        }

                        GPIOR0 = 0;
                        break;
                    }

                    case KEY_DOWN : { //RIGHT
                        collision = 0;

                        for(i = 0; i < 4; i++) { //delete block
                            if(active_block[i][1] < 20) {
                                draw_block_pattern(active_block[i][1], active_block[i][0], 0x0000);
                            }
                        }

                        for(i = 0; i < 4; i++) { //test for collision with other block
                            if(is_block(active_block[i][1], active_block[i][0] + 1)) {
                                collision = 1;
                            }
                            if((active_block[i][0] + 1) > 9) {
                                collision = 1;
                            }
                        }

                        if(collision == 0) { //draw block
                            for(i = 0; i < 4; i++) { //at new position if available
                                active_block[i][0] += 1;
                                if(active_block[i][1] < 20) {
                                    draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
                                }
                            }
                        } else {
                            for(i = 0; i < 4; i++) { //at the same position
                                if(active_block[i][1] < 20) {
                                    draw_block_pattern(active_block[i][1], active_block[i][0], 0x01FF);
                                }
                            }
                        }

                        GPIOR0 = 0;
                        break;
                    }

                    case KEY_L_FUNC : {
                        GPIOR0 = 0; //Clear after read!

                        if(!pause(score)) {
                            return;
                        }

                        break;
                    }

                    case KEY_R_FUNC : {
                        GPIOR0 = 0; //Clear after read!

                        /*speed_counter = 0;
                        lvl++;
                        if(lvl == 5) {
                            lvl = 0;
                        }
                        */

                        /*
                        GPIOR0 = 0;
                        uint8_t x = 0, y = 0;
                        while(1) {
                            if(GPIOR0 > 0) {
                                pixel_off(x,y);
                                switch(GPIOR0) {
                                    case KEY_LEFT : {
                                        x -= 1;
                                        GPIOR0 = 0;
                                        break;
                                    }

                                    case KEY_RIGHT : {
                                        x += 1;
                                        GPIOR0 = 0;
                                        break;
                                    }

                                    case KEY_UP : {
                                        y -= 1;
                                        GPIOR0 = 0;
                                        break;
                                    }

                                    case KEY_DOWN : {
                                        y += 1;
                                        GPIOR0 = 0;
                                        break;
                                    }

                                    default : {
                                        GPIOR0 = 0;
                                        return;
                                    }
                                }
                                pixel_on(x,y);
                            }
                        }

                        break;
                        */
                    }
                }
            }

        }

        //check for block above top line
        for(k = 0; k < 4; k++) {
            if(active_block[k][1] > 19) {
                game_over(score, 1);
                return;
            }
        }

        //check for whole lines
        for(i = 0; i < 20; i++) {
            full = 1;
            for(k = 0; k < 10; k++) {
                if(!is_block(i, k)) { //find full row
                    full = 0;
                }
            }

            if(full == 1) { //if there is full row
                score += lvl; //add score
                lvl_counter++; //only for deciding which level is player

                if((lvl_counter % 10) == 0) {
                    lvl++;
                    if(lvl > 14) {
                        lvl = 14;
                    }
                }

                for(k = 0; k < 10; k++) { //delete full row
                    draw_block_pattern(i, k, 0x0000);
                }

                for(l = i + 1; l < 20; l++) { //move other rows down by 1
                    for(k = 0; k < 10; k++) {
                        if(is_block(l, k)) { // move only full blocks
                            draw_block_pattern(l, k, 0x0000);
                            draw_block_pattern(l - 1, k, 0x01FF);
                        }
                    }
                }
                i -= 1;
            }
        }

    }

    while(GPIOR0 == 0);
    GPIOR0 = 0;
}

//MENU univ
uint8_t menu(uint8_t items, uint8_t max_length, char descr[items][max_length], uint8_t length[items], uint8_t left_arrow, uint8_t start_pos) {
    //static int8_t start = 1;//, items = 3, left_arrow = 2, positions[3] = {6,9,7};
    int8_t selected = 0;

    clear_video_ram();

    for(uint8_t i = 0; i < items; i++) {
        print_s(descr[i], left_arrow + 1, start_pos + i);
    }

    print_s(">", left_arrow, start_pos);
    print_s("<", length[selected], start_pos);

    while(1) {
        //READING KEYBOARD

        if(GPIOR0 > 0) {
            switch(GPIOR0) {
                case KEY_LEFT : {
                    GPIOR0 = 0;
                    break;
                }

                case KEY_RIGHT : {
                    GPIOR0 = 0;
                    break;
                }

                case KEY_UP : {
                    selected--;
                    if(selected < 0) {
                        selected = items - 1;
                        print_s(" ", left_arrow, start_pos);
                        print_s(" ", length[0], start_pos);
                    } else {
                        print_s(" ", left_arrow, selected + 1 + start_pos);
                        print_s(" ", length[selected + 1], selected + 1 + start_pos);
                    }
                    print_s(">", left_arrow, selected + start_pos);
                    print_s("<", length[selected], selected + start_pos);
                    break;
                }

                case KEY_DOWN : {
                    selected++;
                    if(selected > items - 1) {
                        selected = 0;
                        print_s(" ", left_arrow, items - 1 + start_pos);
                        print_s(" ", length[items - 1], items - 1 + start_pos);
                    } else {
                        print_s(" ", left_arrow, selected - 1 + start_pos);
                        print_s(" ", length[selected - 1], selected - 1 + start_pos);
                    }
                    print_s(">", left_arrow, selected + start_pos);
                    print_s("<", length[selected], selected + start_pos);
                    break;
                }

                case KEY_L_FUNC : {
                    GPIOR0 = 0; //Clear after read!
                    return selected;
                    break;
                }

                case KEY_R_FUNC : {
                    GPIOR0 = 0;
                    break;
                }
            }
            GPIOR0 = 0; //Clear after read!
        }

    }
}

void high_score(uint8_t game) {

    char place[3][3] = {{"1."}, {"2."}, {"3."}};

    uint16_t num;
    char cislo[5], name[5];

    clear_video_ram();

    uint8_t i, l;

    for(i = 0; i < 3; i++) {
        num = eeprom_read_word((uint16_t *) (uint16_t)high_score_addresses[game][i]);

        print_s(place[i], 1, 1 + i);

        cislo[0] = (num / 1000) + 0x30;
        num -= ((num / 1000) * 1000);

        cislo[1] = (num / 100) + 0x30;
        num -= ((num / 100) * 100);

        cislo[2] = (num / 10) + 0x30;
        num -= ((num / 10) * 10);

        cislo[3] = num + 0x30;

        cislo[4] = 0;

        print_s(cislo, 9, 1 + i);

        for(l = 0; l < 4; l++) {
            name[l] = eeprom_read_byte((uint8_t *) (uint8_t)high_score_addresses[game][i] + 2 + l);
        }

        name[4] = 0;
        print_s(name, 4, 1 + i);
    }

    while(GPIOR0 != KEY_R_FUNC);
    GPIOR0 = 0;
}

int main(void) {
    //setup USART
    /*
    UBRR0L = 51;
    UCSR0B |= (1 << TXEN0);
    UDR0 = 85; //SEND control byte
    */

    //SETUP SPI
    DDRB |= (1 << PB3) | (1 << PB2) | (1 << PB5);
    SPCR |= (1 << SPE) | (1 << MSTR);

    //Setup output pins
    DDRD |= (1 << PD4) | (1 << PD6) | (1 << PD7);

//Prepare keyboard
    //PORT and pull-up
    MCUCR &= ~(1 << PUD); //Enable internal pull-up
    PORTC |= (1 << PC0) | //PC0 - PC5 - enable pull-up for pins
            (1 << PC1) |
            (1 << PC2) |
            (1 << PC3) |
            (1 << PC4) |
            (1 << PC5);

    //Prepare interrupt for keyboard
    PCICR |= (1 << PCIE1); //enable PCINT for whole PORT C
    PCMSK1 |= (1 << PCINT8) | //enable PCINT for PC0 - PC5 pins
            (1 << PCINT9) |
            (1 << PCINT10) |
            (1 << PCINT11) |
            (1 << PCINT12) |
            (1 << PCINT13);

    //debounce timer for keyboard
    cli();
    TIMSK0 |= (1 << TOIE0);
    sei();

    /** RNG init*/
    //Get seed for this session
    r_y = eeprom_read_word((uint16_t *)0);
    r_x = eeprom_read_word((uint16_t *)2);

    //Generate new seeds
    RNG1();
    RNG2();

    //Save seed for next session
    eeprom_write_word((uint16_t *)0, r_x);
    eeprom_write_word((uint16_t *)2, r_y);

    /** Display init sequence */
    //Perform display reset
    PORTD &= ~(1 << PD6);
    _delay_ms(10);
    PORTD |= (1 << PD6);
    //Display reseted

    //2
    ENABLE_DISP
    DISP_COMMAND
    SPDR = 0b00100001; //FUNCTION SET: PD = 0; V = 0; H = 1
    while(!(SPSR & (1<<SPIF)));
    //3
    SPDR = 0b11001110; //SET VOP(CONTRAST): 78
    while(!(SPSR & (1<<SPIF)));
    //4
    SPDR = 0b00100000; //FUNCTION SET: PD = 0; V = 0; H = 0
    while(!(SPSR & (1<<SPIF)));
    //5
    SPDR = 0b00001100;//0b00001100; //DISPLAY CONTROL: D = 1; E = 0
    while(!(SPSR & (1<<SPIF)));
    //6

    SPDR = 0b01000000; //SET RAM Y = 0
    while(!(SPSR & (1<<SPIF)));
    SPDR = 0b10000000; //SET RAM X = 0
    while(!(SPSR & (1<<SPIF)));

    DISP_DATA
    //Clear display RAM
    for(i = 0; i < 504; i++) {
        SPDR = 0b00000000;
        while(!(SPSR & (1<<SPIF)));
    }

    /** END: Display init sequence */

    //Prepare video refresh timer 1 - 1024 prescale
    cli();
    TCCR1B |= (1 << WGM12) | (1 << WGM13) | (1 << CS12) | (1 << CS10);
    TIMSK1 |= (1 << ICIE1);
    ICR1 = 435; //18 Hz
    //651; //12 Hz
    //325; //24 Hz
    //3907; //2 Hz
    //435; //18 Hz

    //Activate SPI interrupt
    SPCR |= (1 << SPIE);
    sei();


    //Backlight control - testing
    DDRD |= (1 << PD3);
    //PORTD |= (1 << PD3);
    PORTD &= ~(1 << PD3);

    char main_descr[3][7] = {{"Snake"}, {"Tetris"}, {"About"}};
    uint8_t main_length[3] = {8, 9, 8};

    char game_descr[3][11] = {{"Play"}, {"High score"}, {"Back"}};
    uint8_t game_length[3] = {7, 13, 7};

    while (1) {
        switch(menu(3, 7, main_descr, main_length, 2, 1)) {
            case 0 : { //SNAKE
                switch(menu(3, 11, game_descr, game_length, 2, 1)) {
                    case 0 : {
                        snake_main();
                        break;
                    }

                    case 1 : {
                        high_score(0);
                        break;
                    }
                }
                break;
            }

            case 1 : { //TETRIS
                switch(menu(3, 11, game_descr, game_length, 2, 1)) {
                    case 0 : {
                        tetris_main();
                        break;
                    }

                    case 1 : {
                        high_score(1);
                        break;
                    }
                }
                break;
            }

            case 2 : { //ABOUT
                clear_video_ram();

                print_d("   Made by\n  Viktor B.\n\n\n   SN: #06");

                while(GPIOR0 == 0);

                if(GPIOR0 == KEY_LEFT) {
                    GPIOR0 = 0;
                    clear_video_ram();

                    print_d("Reset\nEEPROM");

                    _delay_ms(500);

                    if(GPIOR0 == KEY_RIGHT) {
                        for(uint8_t i = 0; i < 36; i++) {
                            eeprom_write_byte((uint8_t *) (uint8_t)i + 10, 0);
                        }

                        print_d("Reset\nOK      ");

                        _delay_ms(500);
                    }
                }

                GPIOR0 = 0;

                break;
            }
        }
    }

    return 0;
}

/*

//READING KEYBOARD
if(GPIOR0 > 0) {
    switch(GPIOR0) {
        case KEY_LEFT : {

            break;
        }

        case KEY_RIGHT : {

            break;
        }

        case KEY_UP : {

            break;
        }

        case KEY_DOWN : {

            break;
        }

        case KEY_R_FUNC : {

            break;
        }

        case KEY_L_FUNC : {

            break;
        }
    }
    GPIOR0 = 0; //Clear after read!
}



//RNG TEST
while(GPIOR0 == 0) {

        t1 = (uint16_t) RNG1();

        cislo[0] = (t1 / 10000) + 0x30;
        t1 -= ((t1 / 10000) * 10000);

        cislo[1] = (t1 / 1000) + 0x30;
        t1 -= ((t1 / 1000) * 1000);

        cislo[2] = (t1 / 100) + 0x30;
        t1 -= ((t1 / 100) * 100);

        cislo[3] = (t1 / 10) + 0x30;
        t1 -= ((t1 / 10) * 10);

        cislo[4] = t1 + 0x30;

        cislo[5] = 0;

        print_d(cislo);

        _delay_ms(200);
        //draw_block_pattern((uint8_t)((uint16_t)RNG1()/(uint16_t)2330), (uint8_t)((uint16_t)RNG2()/(uint16_t)4100), 0x00AA);
    }
    GPIOR0 = 0;

*/
