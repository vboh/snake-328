#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

/**
    PB2 - CHIP ENABLE
    PC1 - RESET
    PC0 - D/!C
    PD5 - BACKLIGHT - till problem with PB1 and PB0 remains!
*/

#define ENABLE_DISP  PORTB&=~(1<<PB2);
#define DISABLE_DISP PORTB|=(1<<PB2);

#define DISP_DATA PORTC|=(1<<PC0);
#define DISP_COMMAND PORTC&=~(1<<PC0);

volatile uint8_t sending_ram = 0,
                ram_x = 0,
                ram_y = 0,
                video_RAM[6][84],
                x = 4,
                y = 0,
                direction = 0,
                game = 0,
                button = 0,
                down,
                x_l = 0,
                y_l = 0,
                x_f = 0,
                y_f = 0;
volatile uint16_t r_x = 1500,
                  r_y = 16,
                  score = 0;
uint16_t i = 0;
uint8_t j = 0;

void clear_video_ram() {
    for(i = 0; i < 6; i++) {
        for(j = 0; j < 84; j++) {
            video_RAM[i][j] = 0b00000000;
        }
    }
}

void zobraz_pixel(y, x) {
    video_RAM[y/8][x] |= (1 << (y%8));
}

void smaz_pixel(y, x) {
    video_RAM[y/8][x] &= ~(1 << (y%8));
}

ISR(TIMER0_OVF_vect) {
    TCCR0B &= ~((1 << CS02) | (1 << CS00)); //Stop timer
    TCNT0 = 0; //Reset timer

    down = (~PINC & 0b00111100) >> 2;
    if(down == button) {
        switch (down) {
            case 1 : { //LEFT
                direction = 2;
                break;
            }
            case 2 : { //UP
                direction = 3;
                break;
            }
            case 4 : { //DOWN
                direction = 4;
                break;
            }
            case 8 : { //RIGHT
                direction = 1;
                break;
            }
        }
    }
    button = 0;
}

ISR(PCINT1_vect) {
    if(button == 0) {
        down = (~PINC & 0b00111100) >> 2;
        if(down) {
            button = down;
            TCCR0B |= (1 << CS02) | (1 << CS00); //start debounce timer
        }
    }
}

void pixel3_on(uint8_t y, uint8_t x, uint8_t dir) {
    y = (4 * y) + 3;
    x = (4 * x) + 3;

    video_RAM[y/8][x] |= (1 << (y%8));
    video_RAM[y/8][x - 1] |= (1 << (y%8));
    video_RAM[y/8][x + 1] |= (1 << (y%8));

    y++;
    video_RAM[y/8][x] |= (1 << (y%8));
    video_RAM[y/8][x - 1] |= (1 << (y%8));
    video_RAM[y/8][x + 1] |= (1 << (y%8));

    y-=2;
    video_RAM[y/8][x] |= (1 << (y%8));
    video_RAM[y/8][x - 1] |= (1 << (y%8));
    video_RAM[y/8][x + 1] |= (1 << (y%8));

    y++;
    switch(dir) {
        case 1 : { //RIGHT
            x-=2;
            video_RAM[y/8][x] |= (1 << (y%8));
            y++;
            video_RAM[y/8][x] |= (1 << (y%8));
            y-=2;
            video_RAM[y/8][x] |= (1 << (y%8));
            break;
        }

        case 2 : { //LEFT
            x+=2;
            video_RAM[y/8][x] |= (1 << (y%8));
            y++;
            video_RAM[y/8][x] |= (1 << (y%8));
            y-=2;
            video_RAM[y/8][x] |= (1 << (y%8));
            break;
        }

        case 3 : { //UP
            y+=2;
            video_RAM[y/8][x] |= (1 << (y%8));
            x++;
            video_RAM[y/8][x] |= (1 << (y%8));
            x-=2;
            video_RAM[y/8][x] |= (1 << (y%8));
            break;
        }

        case 4 : { //DOWN
            y-=2;
            video_RAM[y/8][x] |= (1 << (y%8));
            x++;
            video_RAM[y/8][x] |= (1 << (y%8));
            x-=2;
            video_RAM[y/8][x] |= (1 << (y%8));
        }
    }
}

void pixel3_off(uint8_t y, uint8_t x) {
    y = (4 * y) + 3;
    x = (4 * x) + 3;

    video_RAM[y/8][x] &= ~(1 << (y%8));
    video_RAM[y/8][x - 1] &= ~(1 << (y%8));
    video_RAM[y/8][x + 1] &= ~(1 << (y%8));

    y++;
    video_RAM[y/8][x] &= ~(1 << (y%8));
    video_RAM[y/8][x - 1] &= ~(1 << (y%8));
    video_RAM[y/8][x + 1] &= ~(1 << (y%8));

    y-=2;
    video_RAM[y/8][x] &= ~(1 << (y%8));
    video_RAM[y/8][x - 1] &= ~(1 << (y%8));
    video_RAM[y/8][x + 1] &= ~(1 << (y%8));

    y++;
    if(video_RAM[y/8][x + 2] & (1 << (y%8))) { //DEL RIGHT
        x+=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        y++;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        y-=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));

        x_l++;
    } else if(video_RAM[y/8][x - 2] & (1 << (y%8))) { //DEL LEFT
        x-=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        y++;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        y-=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));

        x_l--;
    } else if(video_RAM[(y+2)/8][x] & (1 << ((y+2)%8))) { //DEL DOWN
        y+=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        x++;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        x-=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));

        y_l++;
    } else if(video_RAM[(y-2)/8][x] & (1 << ((y-2)%8))) { //DEL UP
        y-=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        x++;
        video_RAM[y/8][x] &= ~(1 << (y%8));
        x-=2;
        video_RAM[y/8][x] &= ~(1 << (y%8));

        y_l--;
    }
}

void food3(uint8_t y, uint8_t x) {
    y = (4 * y) + 3;
    x = (4 * x) + 3;

    video_RAM[y/8][x - 1] |= (1 << (y%8));
    video_RAM[y/8][x + 1] |= (1 << (y%8));

    y++;
    video_RAM[y/8][x] |= (1 << (y%8));

    y-=2;
    video_RAM[y/8][x] |= (1 << (y%8));
}

uint8_t is_snake3(uint8_t y, uint8_t x) {
    y = (4 * y) + 3;
    x = (4 * x) + 3;

    return (video_RAM[y/8][x] & (1 << (y%8)));
}

ISR(SPI_STC_vect) {
    if(sending_ram == 1) {
        SPDR = video_RAM[ram_y][ram_x++];
        if(ram_x == 84) {
            ram_x = 0;
            ram_y++;
            if(ram_y == 6) {
                ram_y = 0;
                ram_x = 0;
                sending_ram = 0;
            }
        }
    }
}

void end_game() {
    game = 0;
    direction = 0;
    video_RAM[1][0] = 0b01010101;
}

void generate_food() {
    do{ //Vygenerování nového jídla mimo hada
        do{
        r_x = (uint16_t)(((uint32_t)r_x * (uint32_t)931 + (uint32_t)12641) % (uint32_t)44640); //Náhodný generátor pro x souøadnici
        x_f = (uint16_t)r_x / (uint16_t)2231; //Pøepoèítání náhodného èísla na souøadnici na obrazovce
        } while(x_f > 19);
        do {
        r_y = (uint16_t)(((uint32_t)r_y * (uint32_t)40 + (uint32_t)9029) % (uint32_t)41067); //Náhodný generátor pro y souøadnici
        y_f = (uint16_t)r_y / (uint16_t)3733; //Pøepoèítání náhodného èísla na souøadnici na obrazovce
        }while(y_f > 10);
    } while (is_snake3(y_f, x_f));
    food3(y_f, x_f); //Vygenerování nového jídla
}

ISR(TIMER1_CAPT_vect) {
    switch(direction) {
        case 1 : { //RIGHT
            x++;
            if(x > 19) {
                end_game();
            }
            break;
        }

        case 2 : { //LEFT
            x--;
            if(x > 200) {
                end_game();
            }
            break;
        }

        case 3 : { //UP
            y--;
            if(y > 200) {
                end_game();
            }
            break;
        }

        case 4 : { //DOWN
            y++;
            if(y > 10) {
                end_game();
            }
            break;
        }
    }

    if(is_snake3(y,x) && (game == 1) && (direction > 0)) {
        end_game();
    }

    if((game == 1) && (direction > 0)) {
        if((x == x_f) && (y_f == y)) { //IF food
            //NEW FOOD
            generate_food();
            score++; //Pøidání bodù
            switch(score) {
                case 5 : {
                    TCNT1 = 0;
                    ICR1 = 2000;
                    break;
                }
            }
        } else {
            //MOVE
            pixel3_off(y_l, x_l);
        }

        pixel3_on(y, x, direction);
    }

    SPDR = video_RAM[ram_y][ram_x++];
    sending_ram = 1;
}

void start_snake() {
    clear_video_ram();

    //Zéobraz rámeček
    for(i = 0; i < 83; i++) {
        zobraz_pixel(0, i);
    }

    for(i = 0; i < 83; i++) {
        zobraz_pixel(46, i);
    }

    for(i = 0; i < 47; i++) {
        zobraz_pixel(i, 0);
    }

    for(i = 0; i < 47; i++) {
        zobraz_pixel(i, 82);
    }

    //Zobraz HADA
    pixel3_on(0, x, 0);
    pixel3_on(0, x-1, 2);
    pixel3_on(0, x-2, 2);
    pixel3_on(0, x-3, 2);
    pixel3_on(0, x-4, 2);

    //New seed
    r_y = eeprom_read_word((uint16_t *)0);
    r_x = eeprom_read_word((uint16_t *)2);

    //Jidlo
    generate_food();

    //Save seed, prohozeni je zamerne, kvuli lepsi nahodnosti
    eeprom_write_word((uint16_t *)0, r_x);
    eeprom_write_word((uint16_t *)2, r_y);

    game = 1;
}

int main(void) {
    //setup USART
    UBRR0L = 51;
    UCSR0B |= (1 << TXEN0);

    UDR0 = 85;

    //SETUP SPI
    DDRB |= 0b00101111;
    SPCR |= (1 << SPE) | (1 << MSTR);

    //Setup output pins
    DDRC |= 0b00000011;
    DDRD |= (1 << PD4);

//Prepare keyboard
    //PORT and pull-up
    MCUCR &= ~(1 << PUD); //Enable internal pull-up
    PORTC |= 0b00111100; //PC2 - PC5

    //Interrupt
    PCICR |= (1 << PCIE1);
    PCMSK1 |= (1 << PCINT10) | (1 << PCINT11) | (1 << PCINT12) | (1 << PCINT13);

    //debounce timer
    cli();
    TIMSK0 |= (1 << TOIE0);
    sei();

    //Perform display reset
    PORTC &= ~(1 << PC1);
    _delay_ms(10);
    PORTC |= (1 << PC1);
    //Display resseted

    //2
    ENABLE_DISP
    SPDR = 0b00100001; //FUNCTION SET: PD = 0; V = 0; H = 1
    while(!(SPSR & (1<<SPIF)));
    //3
    SPDR = 0b11001110; //SET VOP(CONTRAST): 78
    while(!(SPSR & (1<<SPIF)));
    //4
    SPDR = 0b00100000; //FUNCTION SET: PD = 0; V = 0; H = 0
    while(!(SPSR & (1<<SPIF)));
    //5
    SPDR = 0b00001100; //DISPLAY CONTROL: D = 1; E = 0
    while(!(SPSR & (1<<SPIF)));
    //6

    DISP_DATA
    //Clear display
    for(i = 0; i < 504; i++) {
        SPDR = 0b00000000;
        while(!(SPSR & (1<<SPIF)));
    }

    //Prepare timer 1
    cli();
    TCCR1B |= (1 << WGM12) | (1 << WGM13) | (1 << CS12) | (1 << CS10);
    TIMSK1 |= (1 << ICIE1);
    ICR1 = 3907; //2 Hz
    //651; //12 Hz
    //325; //24 Hz
    //3907; //2 Hz

    //Activate SPI interrupt
    SPCR |= (1 << SPIE);
    sei();

    start_snake();

    while(1) {
        if(game == 2) {
            _delay_ms(1000);
            pixel3_off(y_f, x_f);
            generate_food();
        }
    }

    return 0;
}
