<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="15" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="197" name="PracDim" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-lstb" urn="urn:adsk.eagle:library:162">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA03-2" urn="urn:adsk.eagle:footprint:8265/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.175" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-3.81" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="4.064" y="0.635" size="1.27" layer="21" ratio="10">6</text>
<text x="-1.27" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MA03-2" urn="urn:adsk.eagle:package:8334/1" type="box" library_version="1">
<description>PIN HEADER</description>
</package3d>
</packages3d>
<symbols>
<symbol name="MA03-2" urn="urn:adsk.eagle:symbol:8264/1" library_version="1">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA03-2" urn="urn:adsk.eagle:component:8382/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA03-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-2">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8334/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X02" urn="urn:adsk.eagle:footprint:22309/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90" urn="urn:adsk.eagle:footprint:22310/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:22435/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
<package3d name="1X02/90" urn="urn:adsk.eagle:package:22437/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD2" urn="urn:adsk.eagle:symbol:22308/1" library_version="3">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X2" urn="urn:adsk.eagle:component:22516/3" prefix="JP" uservalue="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22435/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22437/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="snake-328-hw-smd">
<packages>
<package name="SOT23_6">
<text x="-1" y="2.03" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-3.27" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="1" y1="1.7" x2="0.4" y2="1.7" width="0.127" layer="21"/>
<wire x1="0.4" y1="1.7" x2="-0.4" y2="1.7" width="0.127" layer="21"/>
<wire x1="-0.4" y1="1.7" x2="-1" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1" y1="1.7" x2="-1" y2="-1.7" width="0.127" layer="21"/>
<wire x1="1" y1="1.7" x2="1" y2="-1.7" width="0.127" layer="21"/>
<wire x1="1" y1="-1.7" x2="-1" y2="-1.7" width="0.127" layer="21"/>
<smd name="1" x="-1.3" y="0.95" dx="1.3" dy="0.7" layer="1" roundness="100"/>
<smd name="3" x="-1.3" y="-0.95" dx="1.3" dy="0.7" layer="1" roundness="100"/>
<smd name="2" x="-1.3" y="0" dx="1.3" dy="0.7" layer="1" roundness="100"/>
<smd name="6" x="1.3" y="0.95" dx="1.3" dy="0.7" layer="1" roundness="100"/>
<smd name="4" x="1.3" y="-0.95" dx="1.3" dy="0.7" layer="1" roundness="100"/>
<smd name="5" x="1.3" y="0" dx="1.3" dy="0.7" layer="1" roundness="100"/>
<wire x1="0.4" y1="1.7" x2="-0.4" y2="1.7" width="0.127" layer="21" curve="-180"/>
</package>
<package name="JS202011SCQN">
<smd name="1" x="-3" y="2.5" dx="3" dy="1.2" layer="1" roundness="100"/>
<smd name="2" x="-3" y="0" dx="3" dy="1.2" layer="1" roundness="100"/>
<smd name="3" x="-3" y="-2.5" dx="3" dy="1.2" layer="1" roundness="100"/>
<smd name="4" x="3" y="2.5" dx="3" dy="1.2" layer="1" roundness="100"/>
<smd name="5" x="3" y="0" dx="3" dy="1.2" layer="1" roundness="100"/>
<smd name="6" x="3" y="-2.5" dx="3" dy="1.2" layer="1" roundness="100"/>
<wire x1="-1.8" y1="4.5" x2="-1.8" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-4.5" x2="1.8" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="-4.5" x2="1.8" y2="4.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="4.5" x2="-1.8" y2="4.5" width="0.127" layer="21"/>
<text x="-1.8" y="4.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.8" y="-6.1" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="0.7" y1="2.3" x2="-0.7" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="2.3" x2="-0.7" y2="-0.3" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-0.3" x2="-0.7" y2="-0.8" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-0.8" x2="-0.7" y2="-1.3" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-1.3" x2="-0.7" y2="-1.8" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-1.8" x2="-0.7" y2="-2.3" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-2.3" x2="0.7" y2="-2.3" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-2.3" x2="0.7" y2="-1.8" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-1.8" x2="0.7" y2="-1.3" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-1.3" x2="0.7" y2="-0.8" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-0.8" x2="0.7" y2="-0.3" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-0.3" x2="0.7" y2="2.3" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-0.8" x2="0.7" y2="-0.8" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-1.3" x2="0.7" y2="-1.3" width="0.1524" layer="21"/>
<wire x1="-0.7" y1="-1.8" x2="0.7" y2="-1.8" width="0.1524" layer="21"/>
</package>
<package name="SMD_TACT_SWITCH">
<smd name="1" x="-2.25" y="5" dx="0.8" dy="1.7" layer="1" roundness="100"/>
<smd name="2" x="-2.25" y="-5" dx="0.8" dy="1.7" layer="1" roundness="100"/>
<smd name="4" x="2.25" y="5" dx="0.8" dy="1.7" layer="1" roundness="100"/>
<smd name="3" x="2.25" y="-5" dx="0.8" dy="1.7" layer="1" roundness="100"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.1524" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.1524" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.1524" layer="21"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.7029375" width="0.1524" layer="21"/>
<text x="-3.3" y="-3" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="TQFP_32">
<smd name="1" x="-4.4" y="2.8" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="2" x="-4.4" y="2" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="3" x="-4.4" y="1.2" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="4" x="-4.4" y="0.4" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="5" x="-4.4" y="-0.4" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="6" x="-4.4" y="-1.2" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="7" x="-4.4" y="-2" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="8" x="-4.4" y="-2.8" dx="2" dy="0.55" layer="1" roundness="100"/>
<smd name="9" x="-2.8" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="-2" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="-1.2" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="-0.4" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="0.4" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="1.2" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="2" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="2.8" y="-4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="4.4" y="-2.8" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="18" x="4.4" y="-2" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="19" x="4.4" y="-1.2" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="20" x="4.4" y="-0.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="21" x="4.4" y="0.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="22" x="4.4" y="1.2" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="23" x="4.4" y="2" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="24" x="4.4" y="2.8" dx="2" dy="0.55" layer="1" roundness="100" rot="R180"/>
<smd name="25" x="2.8" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<smd name="26" x="2" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<smd name="27" x="1.2" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<smd name="28" x="0.4" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<smd name="29" x="-0.4" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<smd name="30" x="-1.2" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<smd name="31" x="-2" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<smd name="32" x="-2.8" y="4.4" dx="2" dy="0.55" layer="1" roundness="100" rot="R270"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.1524" layer="51"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.1524" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.1524" layer="51"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.1524" layer="51"/>
<circle x="-5.1" y="3.9" radius="0.14141875" width="0.3048" layer="51"/>
<circle x="-2.2" y="2.2" radius="0.22360625" width="0.508" layer="51"/>
<text x="-2.7" y="0.03" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.7" y="-1.97" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0603">
<smd name="1" x="-0.85" y="0" dx="1.2" dy="1" layer="1" roundness="25"/>
<smd name="2" x="0.85" y="0" dx="1.2" dy="1" layer="1" roundness="25"/>
<wire x1="-1.6" y1="0.65" x2="1.6" y2="0.65" width="0.05" layer="21"/>
<wire x1="1.6" y1="0.65" x2="1.6" y2="-0.65" width="0.05" layer="21"/>
<wire x1="1.6" y1="-0.65" x2="-1.6" y2="-0.65" width="0.05" layer="21"/>
<wire x1="-1.6" y1="-0.65" x2="-1.6" y2="0.65" width="0.05" layer="21"/>
<text x="-1.6" y="0.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<smd name="1" x="-1.55" y="0" dx="1.6" dy="1.9" layer="1" roundness="25"/>
<smd name="2" x="1.55" y="0" dx="1.6" dy="1.9" layer="1" roundness="25"/>
<wire x1="-2.5" y1="1.1" x2="2.5" y2="1.1" width="0.05" layer="21"/>
<wire x1="2.5" y1="1.1" x2="2.5" y2="-1.1" width="0.05" layer="21"/>
<wire x1="2.5" y1="-1.1" x2="-2.5" y2="-1.1" width="0.05" layer="21"/>
<wire x1="-2.5" y1="-1.1" x2="-2.5" y2="1.1" width="0.05" layer="21"/>
<text x="-2.5" y="1.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-2.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805">
<smd name="1" x="-1.02" y="0" dx="1.4" dy="1.4" layer="1" roundness="25"/>
<smd name="2" x="1.02" y="0" dx="1.4" dy="1.4" layer="1" roundness="25"/>
<wire x1="-1.87" y1="0.85" x2="1.87" y2="0.85" width="0.05" layer="21"/>
<wire x1="1.87" y1="0.85" x2="1.87" y2="-0.85" width="0.05" layer="21"/>
<wire x1="1.87" y1="-0.85" x2="-1.87" y2="-0.85" width="0.05" layer="21"/>
<wire x1="-1.87" y1="-0.85" x2="-1.87" y2="0.85" width="0.05" layer="21"/>
<text x="-1.9" y="1.1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9" y="-2.29" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0402">
<smd name="1" x="-0.6" y="0" dx="0.9" dy="0.7" layer="1" roundness="25"/>
<smd name="2" x="0.6" y="0" dx="0.9" dy="0.7" layer="1" roundness="25"/>
<wire x1="-1.2" y1="0.5" x2="1.2" y2="0.5" width="0.05" layer="21"/>
<wire x1="1.2" y1="0.5" x2="1.2" y2="-0.5" width="0.05" layer="21"/>
<wire x1="1.2" y1="-0.5" x2="-1.2" y2="-0.5" width="0.05" layer="21"/>
<wire x1="-1.2" y1="-0.5" x2="-1.2" y2="0.5" width="0.05" layer="21"/>
<text x="-1.2" y="0.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.2" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1210">
<smd name="1" x="-1.45" y="0" dx="2.7" dy="1.2" layer="1" roundness="25" rot="R90"/>
<smd name="2" x="1.45" y="0" dx="2.7" dy="1.2" layer="1" roundness="25" rot="R90"/>
<wire x1="-2.2" y1="1.5" x2="2.2" y2="1.5" width="0.05" layer="21"/>
<wire x1="2.2" y1="1.5" x2="2.2" y2="-1.5" width="0.05" layer="21"/>
<wire x1="2.2" y1="-1.5" x2="-2.2" y2="-1.5" width="0.05" layer="21"/>
<wire x1="-2.2" y1="-1.5" x2="-2.2" y2="1.5" width="0.05" layer="21"/>
<text x="-2.2" y="1.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SMD_1X8_PINSTRIP">
<smd name="P$1" x="-1.5" y="8.89" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<smd name="P$2" x="1.5" y="6.35" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<smd name="P$3" x="-1.5" y="3.81" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<smd name="P$4" x="1.5" y="1.27" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<smd name="P$5" x="-1.5" y="-1.27" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<smd name="P$6" x="1.5" y="-3.81" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<smd name="P$7" x="-1.5" y="-6.35" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<smd name="P$8" x="1.5" y="-8.89" dx="2.4" dy="0.7" layer="1" roundness="100"/>
<text x="-2.7" y="9.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.7" y="-10.8" size="1.27" layer="27">&gt;VALUE</text>
<circle x="0" y="8.9" radius="0.1" width="0.254" layer="21"/>
<circle x="0" y="6.4" radius="0.1" width="0.254" layer="21"/>
<circle x="0" y="3.8" radius="0.1" width="0.254" layer="21"/>
<circle x="0" y="1.3" radius="0.1" width="0.254" layer="21"/>
<circle x="0" y="-1.3" radius="0.1" width="0.254" layer="21"/>
<circle x="0" y="-3.8" radius="0.1" width="0.254" layer="21"/>
<circle x="0" y="-6.4" radius="0.1" width="0.254" layer="21"/>
<circle x="0" y="-8.9" radius="0.1" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MCP1640CT-I/CHY">
<wire x1="-13.208" y1="5.08" x2="-14.2748" y2="5.588" width="0.4064" layer="94"/>
<wire x1="-13.208" y1="5.08" x2="-14.2748" y2="4.572" width="0.4064" layer="94"/>
<wire x1="-13.208" y1="0" x2="-14.2748" y2="0.508" width="0.4064" layer="94"/>
<wire x1="-13.208" y1="0" x2="-14.2748" y2="-0.508" width="0.4064" layer="94"/>
<wire x1="-13.208" y1="-2.54" x2="-14.2748" y2="-2.032" width="0.4064" layer="94"/>
<wire x1="-13.208" y1="-2.54" x2="-14.2748" y2="-3.048" width="0.4064" layer="94"/>
<wire x1="13.208" y1="5.588" x2="14.2748" y2="5.08" width="0.4064" layer="94"/>
<wire x1="13.208" y1="4.572" x2="14.2748" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="12.7" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="10.16" width="0.4064" layer="94"/>
<wire x1="12.7" y1="10.16" x2="-12.7" y2="10.16" width="0.4064" layer="94"/>
<pin name="EN" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="GND" x="-17.78" y="-10.16" length="middle" direction="pas"/>
<pin name="SW" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="VFB" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="VIN" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="VOUT" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<text x="-4.9276" y="12.573" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.1054" y="-19.9136" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="SW">
<circle x="0" y="2.54" radius="0.3592" width="0.2032" layer="94"/>
<circle x="0" y="-2.54" radius="0.3592" width="0.2032" layer="94"/>
<circle x="-2.54" y="0" radius="0.3592" width="0.2032" layer="94"/>
<wire x1="-2.54" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-2.54" width="0.127" layer="94"/>
<wire x1="0" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-1.27" y="3.048" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="-1.524" y="-3.302" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="PUSH_BUTTON">
<circle x="-2.54" y="0" radius="0.359209375" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.359209375" width="0.254" layer="94"/>
<wire x1="-2.794" y1="0.762" x2="2.794" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.794" y1="0.762" x2="2.794" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.794" y1="1.27" x2="-2.794" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.794" y1="1.27" x2="0.762" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="2.032" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.032" x2="-0.762" y2="2.032" width="0.254" layer="94"/>
<wire x1="-0.762" y1="2.032" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.27" x2="-2.794" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.048" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="point" swaplevel="1"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="ATMEGA328PB-32PIN_NOPAD">
<wire x1="-27.94" y1="35.56" x2="25.4" y2="35.56" width="0.254" layer="94"/>
<wire x1="25.4" y1="35.56" x2="25.4" y2="-35.56" width="0.254" layer="94"/>
<wire x1="25.4" y1="-35.56" x2="-27.94" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-35.56" x2="-27.94" y2="35.56" width="0.254" layer="94"/>
<pin name="PE2(ADC6/ICP3/!SS1!)" x="30.48" y="-30.48" length="middle" rot="R180"/>
<pin name="PE3(ADC7/T3/MOSI1)" x="30.48" y="-33.02" length="middle" rot="R180"/>
<pin name="AGND@21" x="-33.02" y="-20.32" length="middle"/>
<pin name="AREF" x="-33.02" y="17.78" length="middle"/>
<pin name="AVCC" x="-33.02" y="27.94" length="middle"/>
<pin name="GND@5" x="-33.02" y="-22.86" length="middle"/>
<pin name="PB0(ICP1/CLKO)" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="PB1(OC1A)" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="PB2(!SS0!/OC1B)" x="30.48" y="-12.7" length="middle" rot="R180"/>
<pin name="PB3(MOSI0/TXD1/OC2A)" x="30.48" y="-15.24" length="middle" rot="R180"/>
<pin name="PB4(MISO0/RXD1)" x="30.48" y="-17.78" length="middle" rot="R180"/>
<pin name="PB5(SCK0/XCK1)" x="30.48" y="-20.32" length="middle" rot="R180"/>
<pin name="PB6(XTAL1/TOSC1)" x="-33.02" y="7.62" length="middle"/>
<pin name="PB7(XTAL2/TOSC2)" x="-33.02" y="2.54" length="middle"/>
<pin name="PC0(ADC0/MISO1)" x="30.48" y="33.02" length="middle" rot="R180"/>
<pin name="PC1(ADC1/SCK1)" x="30.48" y="30.48" length="middle" rot="R180"/>
<pin name="PC2(ADC2)" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="PC3(ADC3)" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA0)" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL0)" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="PC6(!RESET!)" x="-33.02" y="33.02" length="middle" function="dot"/>
<pin name="PD0(RXD/OC3A)" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="PD1(TXD0/OC4A)" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="PD2(INT0/OC3B/OC4B)" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="PD3(INT1/OC2B)" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="PD4(XCK0/T0)" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="PD5(OC0B/T1)" x="30.48" y="2.54" length="middle" rot="R180"/>
<pin name="PD6(OC0A/AIN0)" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="VCC@4" x="-33.02" y="25.4" length="middle"/>
<text x="-27.94" y="36.322" size="1.778" layer="95">&gt;NAME</text>
<text x="-27.94" y="-38.1" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PE0(SDA1/ICP4/ACO)" x="30.48" y="-25.4" length="middle" rot="R180"/>
<pin name="PE1(SCL1/T4)" x="30.48" y="-27.94" length="middle" rot="R180"/>
</symbol>
<symbol name="CAP">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="0" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="0" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="off" length="point" function="dot" swaplevel="1"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="RES">
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-7.62" y="0" visible="off" length="point" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="off" length="point" swaplevel="1" rot="R180"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="L">
<pin name="1" x="-7.62" y="0" visible="off" length="point" direction="pwr" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="off" length="point" direction="pwr" swaplevel="1" rot="R180"/>
<text x="3.81" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="PINHD8">
<wire x1="-6.35" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP1640" prefix="U">
<gates>
<gate name="G$1" symbol="MCP1640CT-I/CHY" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23_6" package="SOT23_6">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SW" pad="1"/>
<connect gate="G$1" pin="VFB" pad="4"/>
<connect gate="G$1" pin="VIN" pad="6"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW" prefix="S">
<gates>
<gate name="G$1" symbol="SW" x="0" y="0"/>
</gates>
<devices>
<device name="JS202011SCQN" package="JS202011SCQN">
<connects>
<connect gate="G$1" pin="1" pad="1 4"/>
<connect gate="G$1" pin="2" pad="2 5"/>
<connect gate="G$1" pin="3" pad="3 6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PUSH_BUTTON" prefix="S">
<gates>
<gate name="G$1" symbol="PUSH_BUTTON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD_TACT_SWITCH">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA328PB" prefix="U">
<gates>
<gate name="G$1" symbol="ATMEGA328PB-32PIN_NOPAD" x="2.54" y="0"/>
</gates>
<devices>
<device name="TQFP_32" package="TQFP_32">
<connects>
<connect gate="G$1" pin="AGND@21" pad="21"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="PB0(ICP1/CLKO)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1A)" pad="13"/>
<connect gate="G$1" pin="PB2(!SS0!/OC1B)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI0/TXD1/OC2A)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO0/RXD1)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK0/XCK1)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1/TOSC1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2/TOSC2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0/MISO1)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1/SCK1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4/SDA0)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5/SCL0)" pad="28"/>
<connect gate="G$1" pin="PC6(!RESET!)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD/OC3A)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD0/OC4A)" pad="31"/>
<connect gate="G$1" pin="PD2(INT0/OC3B/OC4B)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1/OC2B)" pad="1"/>
<connect gate="G$1" pin="PD4(XCK0/T0)" pad="2"/>
<connect gate="G$1" pin="PD5(OC0B/T1)" pad="9"/>
<connect gate="G$1" pin="PD6(OC0A/AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="PE0(SDA1/ICP4/ACO)" pad="3"/>
<connect gate="G$1" pin="PE1(SCL1/T4)" pad="6"/>
<connect gate="G$1" pin="PE2(ADC6/ICP3/!SS1!)" pad="19"/>
<connect gate="G$1" pin="PE3(ADC7/T3/MOSI1)" pad="22"/>
<connect gate="G$1" pin="VCC@4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER_CODE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD_SMD">
<gates>
<gate name="G$1" symbol="PINHD8" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="SMD_1X8_PINSTRIP">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
<connect gate="G$1" pin="7" pad="P$7"/>
<connect gate="G$1" pin="8" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3048" drill="0.3048">
<clearance class="0" value="0.2032"/>
</class>
<class number="1" name="gnd" width="0" drill="0">
<clearance class="1" value="0.3048"/>
</class>
</classes>
<parts>
<part name="ISP" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA03-2" device="" package3d_urn="urn:adsk.eagle:package:8334/1"/>
<part name="U2" library="snake-328-hw-smd" deviceset="MCP1640" device="SOT23_6"/>
<part name="BAT" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X2" device="" package3d_urn="urn:adsk.eagle:package:22435/2"/>
<part name="R1" library="snake-328-hw-smd" deviceset="RES" device="0603" value="510k"/>
<part name="R2" library="snake-328-hw-smd" deviceset="RES" device="0603" value="300k"/>
<part name="C1" library="snake-328-hw-smd" deviceset="CAP" device="0603" value="4.7uF"/>
<part name="C2" library="snake-328-hw-smd" deviceset="CAP" device="0603" value="10uF"/>
<part name="L1" library="snake-328-hw-smd" deviceset="L" device="" value="4.7uF"/>
<part name="C3" library="snake-328-hw-smd" deviceset="CAP" device="0603" value="100n"/>
<part name="S1" library="snake-328-hw-smd" deviceset="SW" device="JS202011SCQN"/>
<part name="S2" library="snake-328-hw-smd" deviceset="PUSH_BUTTON" device=""/>
<part name="S3" library="snake-328-hw-smd" deviceset="PUSH_BUTTON" device=""/>
<part name="S4" library="snake-328-hw-smd" deviceset="PUSH_BUTTON" device=""/>
<part name="S5" library="snake-328-hw-smd" deviceset="PUSH_BUTTON" device=""/>
<part name="S6" library="snake-328-hw-smd" deviceset="PUSH_BUTTON" device=""/>
<part name="S7" library="snake-328-hw-smd" deviceset="PUSH_BUTTON" device=""/>
<part name="U1" library="snake-328-hw-smd" deviceset="ATMEGA328PB" device="TQFP_32"/>
<part name="XA1" library="snake-328-hw-smd" deviceset="PINHD_SMD" device=""/>
<part name="XA2" library="snake-328-hw-smd" deviceset="PINHD_SMD" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="ISP" gate="1" x="30.48" y="-38.1" smashed="yes">
<attribute name="VALUE" x="26.67" y="-45.72" size="1.778" layer="96"/>
<attribute name="NAME" x="26.67" y="-32.258" size="1.778" layer="95"/>
</instance>
<instance part="U2" gate="G$1" x="33.02" y="106.68" smashed="yes">
<attribute name="NAME" x="28.0924" y="119.253" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="27.9146" y="86.7664" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="BAT" gate="G$1" x="-50.8" y="106.68" smashed="yes" rot="MR0">
<attribute name="NAME" x="-44.45" y="112.395" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-44.45" y="101.6" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R1" gate="G$1" x="-5.08" y="83.82" smashed="yes">
<attribute name="NAME" x="-8.89" y="85.3186" size="1.778" layer="95"/>
<attribute name="VALUE" x="-8.89" y="80.518" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="15.24" y="83.82" smashed="yes">
<attribute name="NAME" x="11.43" y="85.3186" size="1.778" layer="95"/>
<attribute name="VALUE" x="11.43" y="80.518" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="-12.7" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="-12.319" y="115.316" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-17.399" y="120.396" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C2" gate="G$1" x="53.34" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="53.721" y="105.156" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="48.641" y="110.236" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="L1" gate="G$1" x="2.54" y="106.68" smashed="yes">
<attribute name="NAME" x="1.016" y="108.9406" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.81" y="103.378" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="-12.7" y="53.34" smashed="yes" rot="R270">
<attribute name="NAME" x="-12.319" y="51.816" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-17.399" y="56.896" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="S1" gate="G$1" x="-20.32" y="109.22" smashed="yes">
<attribute name="NAME" x="-21.59" y="112.268" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-21.844" y="105.918" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S2" gate="G$1" x="71.12" y="78.74" smashed="yes">
<attribute name="NAME" x="68.58" y="81.28" size="1.778" layer="95"/>
</instance>
<instance part="S3" gate="G$1" x="71.12" y="71.12" smashed="yes">
<attribute name="NAME" x="68.58" y="73.66" size="1.778" layer="95"/>
</instance>
<instance part="S4" gate="G$1" x="71.12" y="63.5" smashed="yes">
<attribute name="NAME" x="68.58" y="66.04" size="1.778" layer="95"/>
</instance>
<instance part="S5" gate="G$1" x="71.12" y="55.88" smashed="yes">
<attribute name="NAME" x="68.58" y="58.42" size="1.778" layer="95"/>
</instance>
<instance part="S6" gate="G$1" x="71.12" y="48.26" smashed="yes">
<attribute name="NAME" x="68.58" y="50.8" size="1.778" layer="95"/>
</instance>
<instance part="S7" gate="G$1" x="71.12" y="40.64" smashed="yes">
<attribute name="NAME" x="68.58" y="43.18" size="1.778" layer="95"/>
</instance>
<instance part="U1" gate="G$1" x="22.86" y="33.02" smashed="yes">
<attribute name="NAME" x="-5.08" y="69.342" size="1.778" layer="95"/>
<attribute name="VALUE" x="-5.08" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="XA1" gate="G$1" x="99.06" y="25.4" smashed="yes">
<attribute name="NAME" x="92.71" y="38.735" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.71" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="XA2" gate="G$1" x="129.54" y="25.4" smashed="yes">
<attribute name="NAME" x="123.19" y="38.735" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.19" y="12.7" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<wire x1="96.52" y1="35.56" x2="86.36" y2="35.56" width="0.1524" layer="91"/>
<label x="86.36" y="35.56" size="1.778" layer="95"/>
<pinref part="XA1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="127" y1="35.56" x2="116.84" y2="35.56" width="0.1524" layer="91"/>
<label x="116.84" y="35.56" size="1.778" layer="95"/>
<pinref part="XA2" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-12.7" y1="10.16" x2="-17.78" y2="10.16" width="0.1524" layer="91"/>
<label x="-17.78" y="10.16" size="1.778" layer="95"/>
<wire x1="-10.16" y1="12.7" x2="-12.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="10.16" x2="-10.16" y2="10.16" width="0.1524" layer="91"/>
<junction x="-12.7" y="10.16"/>
<pinref part="U1" gate="G$1" pin="AGND@21"/>
<pinref part="U1" gate="G$1" pin="GND@5"/>
</segment>
<segment>
<pinref part="ISP" gate="1" pin="6"/>
<wire x1="22.86" y1="-35.56" x2="15.24" y2="-35.56" width="0.1524" layer="91"/>
<label x="15.24" y="-35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BAT" gate="G$1" pin="2"/>
<wire x1="-48.26" y1="106.68" x2="-38.1" y2="106.68" width="0.1524" layer="91"/>
<label x="-43.18" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="96.52" x2="7.62" y2="96.52" width="0.1524" layer="91"/>
<label x="7.62" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="22.86" y1="83.82" x2="27.94" y2="83.82" width="0.1524" layer="91"/>
<label x="22.86" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="121.92" x2="-5.08" y2="121.92" width="0.1524" layer="91"/>
<label x="-10.16" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="53.34" y1="101.6" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<label x="58.42" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="48.26" x2="-20.32" y2="48.26" width="0.1524" layer="91"/>
<label x="-20.32" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="76.2" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="78.74" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<wire x1="76.2" y1="71.12" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<wire x1="78.74" y1="71.12" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
<wire x1="76.2" y1="63.5" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="78.74" y1="63.5" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<junction x="78.74" y="71.12"/>
<wire x1="76.2" y1="55.88" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="55.88" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<junction x="78.74" y="63.5"/>
<wire x1="76.2" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<wire x1="78.74" y1="48.26" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<junction x="78.74" y="55.88"/>
<wire x1="76.2" y1="40.64" x2="78.74" y2="40.64" width="0.1524" layer="91"/>
<wire x1="78.74" y1="40.64" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<junction x="78.74" y="48.26"/>
<label x="78.74" y="78.74" size="1.778" layer="95"/>
<pinref part="S2" gate="G$1" pin="2"/>
<pinref part="S3" gate="G$1" pin="2"/>
<pinref part="S4" gate="G$1" pin="2"/>
<pinref part="S5" gate="G$1" pin="2"/>
<pinref part="S6" gate="G$1" pin="2"/>
<pinref part="S7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="53.34" y1="33.02" x2="68.58" y2="33.02" width="0.1524" layer="91"/>
<wire x1="68.58" y1="33.02" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="17.78" x2="96.52" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PD6(OC0A/AIN0)"/>
<pinref part="XA1" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="53.34" y1="20.32" x2="96.52" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB2(!SS0!/OC1B)"/>
<pinref part="XA1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="53.34" y1="30.48" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<wire x1="71.12" y1="30.48" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
<wire x1="71.12" y1="22.86" x2="96.52" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PD7(AIN1)"/>
<pinref part="XA1" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="53.34" y1="17.78" x2="66.04" y2="17.78" width="0.1524" layer="91"/>
<wire x1="66.04" y1="17.78" x2="66.04" y2="25.4" width="0.1524" layer="91"/>
<pinref part="ISP" gate="1" pin="4"/>
<wire x1="22.86" y1="-38.1" x2="10.16" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-38.1" x2="10.16" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-45.72" x2="66.04" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-45.72" x2="66.04" y2="17.78" width="0.1524" layer="91"/>
<junction x="66.04" y="17.78"/>
<wire x1="66.04" y1="25.4" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB3(MOSI0/TXD1/OC2A)"/>
<pinref part="XA1" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="53.34" y1="12.7" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="12.7" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<wire x1="63.5" y1="12.7" x2="63.5" y2="27.94" width="0.1524" layer="91"/>
<pinref part="ISP" gate="1" pin="3"/>
<wire x1="38.1" y1="-38.1" x2="58.42" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-38.1" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<junction x="58.42" y="12.7"/>
<wire x1="63.5" y1="27.94" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB5(SCK0/XCK1)"/>
<pinref part="XA1" gate="G$1" pin="4"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="ISP" gate="1" pin="2"/>
<wire x1="22.86" y1="-40.64" x2="15.24" y2="-40.64" width="0.1524" layer="91"/>
<label x="15.24" y="-40.64" size="1.778" layer="95"/>
</segment>
<segment>
<label x="-17.78" y="58.42" size="1.778" layer="95"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="58.42" x2="-12.7" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="58.42" x2="-17.78" y2="58.42" width="0.1524" layer="91"/>
<junction x="-12.7" y="58.42"/>
<pinref part="U1" gate="G$1" pin="VCC@4"/>
</segment>
<segment>
<wire x1="96.52" y1="30.48" x2="86.36" y2="30.48" width="0.1524" layer="91"/>
<label x="86.36" y="30.48" size="1.778" layer="95"/>
<pinref part="XA1" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VOUT"/>
<label x="53.34" y="111.76" size="1.778" layer="95"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="53.34" y1="111.76" x2="58.42" y2="111.76" width="0.1524" layer="91"/>
<junction x="53.34" y="111.76"/>
<wire x1="50.8" y1="111.76" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="83.82" x2="-17.78" y2="83.82" width="0.1524" layer="91"/>
<label x="-17.78" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="ISP" gate="1" pin="1"/>
<wire x1="38.1" y1="-40.64" x2="60.96" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-40.64" x2="60.96" y2="15.24" width="0.1524" layer="91"/>
<wire x1="60.96" y1="15.24" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB4(MISO0/RXD1)"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="ISP" gate="1" pin="5"/>
<wire x1="38.1" y1="-35.56" x2="40.64" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-35.56" x2="40.64" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-30.48" x2="-22.86" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-30.48" x2="-22.86" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="66.04" x2="-10.16" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PC6(!RESET!)"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="55.88" y1="66.04" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<wire x1="55.88" y1="78.74" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<wire x1="55.88" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="PC0(ADC0/MISO1)"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="66.04" y1="71.12" x2="58.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="58.42" y1="71.12" x2="58.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="58.42" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S3" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="PC1(ADC1/SCK1)"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="53.34" y1="60.96" x2="60.96" y2="60.96" width="0.1524" layer="91"/>
<wire x1="60.96" y1="60.96" x2="60.96" y2="63.5" width="0.1524" layer="91"/>
<wire x1="60.96" y1="63.5" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S4" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="PC2(ADC2)"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="66.04" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
<wire x1="60.96" y1="55.88" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
<wire x1="60.96" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<pinref part="S5" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="PC3(ADC3)"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="53.34" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="58.42" y1="55.88" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="48.26" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<pinref part="S6" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="PC4(ADC4/SDA0)"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="66.04" y1="40.64" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<wire x1="55.88" y1="40.64" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
<wire x1="55.88" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<pinref part="S7" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="PC5(ADC5/SCL0)"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VIN"/>
<pinref part="U2" gate="G$1" pin="EN"/>
<wire x1="-12.7" y1="111.76" x2="-7.62" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="111.76" x2="12.7" y2="111.76" width="0.1524" layer="91"/>
<wire x1="12.7" y1="111.76" x2="15.24" y2="111.76" width="0.1524" layer="91"/>
<wire x1="15.24" y1="104.14" x2="12.7" y2="104.14" width="0.1524" layer="91"/>
<wire x1="12.7" y1="104.14" x2="12.7" y2="111.76" width="0.1524" layer="91"/>
<junction x="12.7" y="111.76"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="106.68" x2="-7.62" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="106.68" x2="-7.62" y2="111.76" width="0.1524" layer="91"/>
<junction x="-7.62" y="111.76"/>
<wire x1="-12.7" y1="111.76" x2="-17.78" y2="111.76" width="0.1524" layer="91"/>
<junction x="-12.7" y="111.76"/>
<pinref part="S1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="FB" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VFB"/>
<wire x1="15.24" y1="101.6" x2="5.08" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="2.54" y1="83.82" x2="5.08" y2="83.82" width="0.1524" layer="91"/>
<wire x1="5.08" y1="83.82" x2="5.08" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="7.62" y1="83.82" x2="5.08" y2="83.82" width="0.1524" layer="91"/>
<junction x="5.08" y="83.82"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SW"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="10.16" y1="106.68" x2="15.24" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="BAT" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="109.22" x2="-25.4" y2="109.22" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
